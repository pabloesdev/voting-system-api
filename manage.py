import os
import sys
import click
import subprocess
from dotenv import load_dotenv


def check_mandatory_env_variables():
    mandatory_env_variables = ("DATABASE_URL",)
    for env in mandatory_env_variables:
        if os.getenv(env) is None:
            raise Exception(f"`{env}` environment variable is not set")


def load_env(env: str) -> None:
    dotenv_file = ".env"
    if env != "local":
        dotenv_file = f".env.{env}"
    load_dotenv(dotenv_file)
    check_mandatory_env_variables()


def check_uri() -> bool:
    db_uri_check = os.getenv("DATABASE_APPLICATION")
    return db_uri_check is not None


def validate_env(_context: click.Context, _parameter: click.Option | click.Parameter, env: str) -> str:
    values = ("local", "test", "dev", "stg", "prod")
    if env not in values:
        raise click.BadParameter(f"`env` must be one of: {values}")
    if not check_uri():
        load_env(env)
    return env


@click.group("App CLI")
def cli() -> None:
    pass

@cli.group(help="Run commands")
def run() -> None:
    pass


@run.command(help="Run the web server")
@click.argument("env", envvar="ENV", default="local", callback=validate_env)
@click.option("--host", "-h", default="0.0.0.0")
@click.option("--port", "-p", default="5000")
def server(env: str, host: str, port: int):
    try:
        load_env(env)
        subprocess.check_call(["python", "manage.py", "check", "styles"])
        subprocess.check_call(["python", "manage.py", "check", "types"])
        subprocess.check_call(['python', 'manage.py', 'run', 'tests'])
        return subprocess.call([
            "uvicorn",
            "src.main:app",
            "--reload",
            "--host",
            f"{host}",
            "--port",
            f"{port}",
        ])
    except subprocess.CalledProcessError as cpe:
        raise Exception(str(cpe))

@run.command(help="Run tests, add --watch to keep running")
@click.argument('env', envvar='ENV', default='local', callback=validate_env)
@click.option('--watch', '-w', is_flag=True, help='to keep running')
def tests(watch: bool, env: str) -> int:
    try:
        load_env(env)
        click.echo('Running `pytest`...')
        if watch:
            return subprocess.check_call('ptw')
        return subprocess.check_call('pytest')
    except subprocess.CalledProcessError as cpe:
        raise Exception(str(cpe))



@cli.group(help="Check code styles ")
def check() -> None:
    pass

@check.command(help="Run the linter")
def styles() -> int:
    try:
        click.echo("Running `flake8`...")
        return subprocess.check_call(["flake8", "--count"])
    except subprocess.CalledProcessError as cpe:
        raise Exception(str(cpe))
    
@check.command(help="Run the type checker")
def types() -> int:
    try:
        click.echo("Running `mypy`...")
        return subprocess.check_call(["mypy", "--show-error-codes"])
    except subprocess.CalledProcessError as cpe:
        raise Exception(str(cpe))


if __name__ == "__main__":
    cli()
