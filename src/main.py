from src.app import Application
from src.configuration import configure_inject
from src.presentation.server import FastAPIApplication


def create_application() -> Application:
    application: Application = FastAPIApplication()
    application.add_settings()
    application.register_routers()
    return application


configure_inject()
app = create_application().app
