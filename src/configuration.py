import inject
from src.domain.candidate.candidate_by_election_impl import CandidateByElectionFinderImpl
from src.domain.candidate.candidate_by_id_finder_impl import CandidateByIdFinderImpl
from src.domain.candidate.ports.input.candidate_by_election_finder import CandidateByElectionFinder
from src.domain.candidate.ports.input.candidate_by_id_finder import CandidateByIdFinder
from src.domain.candidate.ports.output.candidate_repository import CandidateRepository
from src.domain.central_electoral.all_central_electoral_finder_impl import AllCentralElectoralFinderImpl
from src.domain.central_electoral.central_electoral_by_id_finder_impl import CentralElectoralByIdFinderImpl
from src.domain.central_electoral.ports.input.all_central_electoral_finder import AllCentralElectoralFinder
from src.domain.central_electoral.ports.input.central_electoral_by_id_finder import CentralElectoralByIdFinder
from src.domain.central_electoral.ports.output.central_electoral_repository import CentralElectoralRepository
from src.domain.election.all_elections_finder_impl import AllElectionsFinderImpl
from src.domain.election.election_by_id_finder_impl import ElectionByIdFinderImpl
from src.domain.election.ports.input.all_elections_finder import AllElectionsFinder
from src.domain.election.ports.input.election_by_id_finder import ElectionByIdFinder
from src.domain.election.ports.output.election_repository import ElectionRepository
from src.domain.user.ports.input.user_creator import UserCreator
from src.domain.user.ports.input.user_finder import UserFinder

from src.domain.user.ports.output.user_repository import UserRepository
from src.domain.user.user_creator_impl import UserCreatorImpl
from src.domain.user.user_finder_impl import UserFinderImpl
from src.infrastructure.adapters.output.candidate_repository_impl import CandidateRepositoryImpl
from src.infrastructure.adapters.output.central_electoral_repository_impl import CentralElectoralRepositoryImpl
from src.infrastructure.adapters.output.election_repository_impl import ElectionRepositoryImpl
from src.infrastructure.adapters.output.user_repository_impl import UserRepositoryImpl
from src.infrastructure.adapters.output.vote_repository_impl import VoteRepository, VoteRepositoryImpl


def configure_inject() -> None:
    def config_repositories(binder: inject.Binder) -> None:
        binder.bind_to_provider(UserRepository, UserRepositoryImpl)
        binder.bind_to_provider(CandidateRepository, CandidateRepositoryImpl)
        binder.bind_to_provider(ElectionRepository, ElectionRepositoryImpl)
        binder.bind_to_provider(CentralElectoralRepository, CentralElectoralRepositoryImpl)
        binder.bind_to_provider(VoteRepository, VoteRepositoryImpl)

    def config_services(binder: inject.Binder) -> None:
        binder.bind_to_provider(UserCreator, UserCreatorImpl)
        binder.bind_to_provider(UserFinder, UserFinderImpl)
        binder.bind_to_provider(CandidateByIdFinder, CandidateByIdFinderImpl)
        binder.bind_to_provider(CandidateByElectionFinder, CandidateByElectionFinderImpl)
        binder.bind_to_provider(ElectionByIdFinder, ElectionByIdFinderImpl)
        binder.bind_to_provider(AllElectionsFinder, AllElectionsFinderImpl)
        binder.bind_to_provider(AllCentralElectoralFinder, AllCentralElectoralFinderImpl)
        binder.bind_to_provider(CentralElectoralByIdFinder, CentralElectoralByIdFinderImpl)

    def config(binder: inject.Binder) -> None:
        config_repositories(binder)
        config_services(binder)

    inject.configure(config)
