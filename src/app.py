from abc import ABC, abstractmethod
from typing import Any


class Application(ABC):

    """
    Abstract class to defines an application structure
    """

    app: Any = None

    @abstractmethod
    def add_settings(self) -> None:
        raise NotImplementedError()

    @abstractmethod
    def register_routers(self) -> None:
        raise NotImplementedError()

    @abstractmethod
    def run(self, host: str, port: int) -> None:
        raise NotImplementedError()
