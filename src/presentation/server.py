from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
from src.app import Application
from src.presentation.http.v1.auth_controller import auth_router
from src.presentation.http.v1.candidate_controller import candidate_router
from src.presentation.http.v1.election_controller import election_router
from src.presentation.http.v1.central_electoral_controller import central_electoral_router
from src.presentation.http.v1.vote_controller import vote_controller


class FastAPIApplication(Application):

    """
    Implementation to create a FastAPI Application
    """

    def __init__(self) -> None:
        self.app: FastAPI = FastAPI()

    def add_settings(self) -> None:
        origins = [
            '*',
            "http://localhost",
            "http://localhost:3000",
            "https://domain.com",
        ]
        self.app.title = "Voting System API"
        self.app.version = "0.0.1"
        self.app.add_middleware(
            CORSMiddleware,
            allow_origins=origins,
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )

    def register_routers(self) -> None:
        router_prefix = "/api/v1"
        self.app.include_router(prefix=router_prefix, router=auth_router())
        self.app.include_router(prefix=router_prefix, router=candidate_router())
        self.app.include_router(prefix=router_prefix, router=election_router())
        self.app.include_router(prefix=router_prefix, router=central_electoral_router())
        self.app.include_router(prefix=router_prefix, router=vote_controller())

    def run(self, host: str, port: int, reload: bool = False) -> None:
        uvicorn.run(self.app, host=host, port=port)
