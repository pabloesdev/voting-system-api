from fastapi import APIRouter
from fastapi.responses import JSONResponse
import inject
from src.domain.election.ports.input.election_by_id_finder import ElectionByIdFinder
from src.domain.election.ports.input.all_elections_finder import AllElectionsFinder
# from src.domain.candidate.candidate import Candidates, Candidate


@inject.autoparams()
def election_router(
    election_by_id_finder: ElectionByIdFinder,
    all_elections_by_finder: AllElectionsFinder,
) -> APIRouter:

    router: APIRouter = APIRouter(tags=["Elections"], prefix="/elections")

    @router.get('/')
    async def get_elections() -> JSONResponse:
        elections = all_elections_by_finder.find()
        response = [election.model_dump() for election in elections]
        return JSONResponse(status_code=200, content=response)

    @router.get('/{election_id}')
    async def get_election_by_id(election_id: int) -> JSONResponse:
        election = election_by_id_finder.find(election_id)
        return JSONResponse(status_code=200, content=election.model_dump())

    return router
