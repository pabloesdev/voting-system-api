from fastapi import APIRouter
from fastapi.responses import JSONResponse
import inject

from src.domain.user.ports.input.user_creator import UserCreator
from src.domain.user.ports.input.user_finder import UserFinder
from src.domain.user.user import User, UserCreate, UserCredentials
from src.infrastructure.utils.token_manager import TokenManager
# import hashlib


@inject.autoparams()
def auth_router(
    user_creator: UserCreator,
    user_finder: UserFinder,
    token_manager: TokenManager
) -> APIRouter:

    router: APIRouter = APIRouter(tags=["Auth"], prefix="/auth")

    @router.post('/login')
    async def login(credentials: UserCredentials) -> JSONResponse:
        user: User = user_finder.find(credentials)
        token = token_manager.generate_token({"email": user.username})
        return JSONResponse(status_code=200, content={
            "name": user.name, "email": user.username, "role": user.role, "access_token": token})

    @router.post('/register')
    async def register(user: UserCreate) -> JSONResponse:
        user_creator.create(user)
        return JSONResponse(status_code=201, content={"message": "Admin created"})

    @router.post('/reset-password')
    def reset_password() -> str:
        return "Password Changed!"

    return router
