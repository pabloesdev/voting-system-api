from fastapi import APIRouter
from fastapi.responses import JSONResponse
import inject
from src.domain.central_electoral.ports.input.all_central_electoral_finder import AllCentralElectoralFinder
from src.domain.central_electoral.ports.input.central_electoral_by_id_finder import CentralElectoralByIdFinder


@inject.autoparams()
def central_electoral_router(
    central_electoral_by_id_finder: CentralElectoralByIdFinder,
    all_central_electoral_finder: AllCentralElectoralFinder,
) -> APIRouter:

    router: APIRouter = APIRouter(tags=["Central Electoral"], prefix="/central_electorals")

    @router.get('/{central_electoral_id}')
    async def get_central_electoral_by_id(central_electoral_id: int) -> JSONResponse:
        central_electoral = central_electoral_by_id_finder.find(central_electoral_id)
        return JSONResponse(status_code=200, content=central_electoral.model_dump())

    @router.get('/')
    async def get_central_electorals() -> JSONResponse:
        central_electorals = all_central_electoral_finder.find()
        response = [central_electoral.model_dump() for central_electoral in central_electorals]
        return JSONResponse(status_code=200, content=response)

    return router
