from fastapi import APIRouter
from fastapi.responses import JSONResponse
import inject
from src.domain.vote.vote import VoteCreate
from src.infrastructure.adapters.output.vote_repository_impl import VoteRepository
from typing import Optional


@inject.autoparams()
def vote_controller(
    vote_repo: VoteRepository
) -> APIRouter:

    router: APIRouter = APIRouter(tags=["Votes"], prefix="/votes")

    @router.post('/')
    async def add_vote(vote: VoteCreate) -> JSONResponse:
        vote_repo.create(vote)
        return JSONResponse(status_code=201, content="Votes registred")

    @router.get('/')
    async def get_votes(province: Optional[str] = None, only_quick_count: Optional[bool] = None) -> JSONResponse:
        votes_result = vote_repo.get_votes(province, only_quick_count)
        return JSONResponse(status_code=200, content=votes_result)

    return router
