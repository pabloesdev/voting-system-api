from fastapi import APIRouter
from fastapi.responses import JSONResponse
import inject
from src.domain.candidate.ports.input.candidate_by_id_finder import CandidateByIdFinder
from src.domain.candidate.ports.input.candidate_by_election_finder import CandidateByElectionFinder
# from src.domain.candidate.candidate import Candidates, Candidate


@inject.autoparams()
def candidate_router(
    candidate_by_id_finder: CandidateByIdFinder,
    candidate_by_election_finder: CandidateByElectionFinder,
) -> APIRouter:

    router: APIRouter = APIRouter(tags=["Candidates"], prefix="/candidates")

    @router.get('/{candidate_id}')
    async def get_candidates_by_id(candidate_id: int) -> JSONResponse:
        candidate = candidate_by_id_finder.find(candidate_id)
        return JSONResponse(status_code=200, content=candidate.model_dump())

    @router.get('/election/{election_id}')
    async def get_candidates_by_election(election_id: int) -> JSONResponse:
        candidates = candidate_by_election_finder.find(election_id)
        response = [candidate.model_dump() for candidate in candidates]
        return JSONResponse(status_code=200, content=response)

    return router
