from abc import ABC, abstractmethod

from src.domain.user.user import User, UserCreate, UserPasswordUpdate


class UserRepository(ABC):

    @abstractmethod
    def create(self, user: UserCreate) -> None: ...

    @abstractmethod
    def find_by_email(self, email: str) -> User: ...

    @abstractmethod
    def update_password(self, user: UserPasswordUpdate) -> None: ...
