from abc import ABC, abstractmethod

from src.domain.user.user import UserCredentials, User


class UserFinder(ABC):

    @abstractmethod
    def find(self, user: UserCredentials) -> User: ...
