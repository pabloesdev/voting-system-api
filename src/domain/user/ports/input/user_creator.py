from abc import ABC, abstractmethod

from src.domain.user.user import UserCreate


class UserCreator(ABC):

    @abstractmethod
    def create(self, user: UserCreate) -> str: ...
