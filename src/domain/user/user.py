from pydantic import BaseModel
from enum import Enum


class RoleType(str, Enum):
    ADMIN = "admin"
    OPERATOR = "operator"
    REPORT = "report"


class UserBase(BaseModel):
    pass


class UserCreate(UserBase):
    name: str
    username: str
    password: str
    role: RoleType


class User(UserBase):
    id: int
    name: str
    username: str
    password: str
    role: RoleType


class UserUpdate(UserBase):
    name: str


class UserPasswordUpdate(UserBase):
    username: str
    password: str


class UserCredentials(UserBase):
    username: str
    password: str


class AuthUser(UserBase):
    token: str
