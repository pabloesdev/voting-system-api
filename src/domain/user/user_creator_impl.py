import inject
from src.domain.user.ports.input.user_creator import UserCreator
from src.domain.user.ports.output.user_repository import UserRepository
from src.domain.user.user import UserCreate


class UserCreatorImpl(UserCreator):

    @inject.autoparams()
    def __init__(self, user_repository: UserRepository) -> None:
        self._user_repository = user_repository

    def create(self, user: UserCreate) -> None:
        self._user_repository.create(user)
