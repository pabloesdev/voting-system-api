import inject
from src.domain.user.ports.input.user_finder import UserFinder
from src.domain.user.ports.output.user_repository import UserRepository
from src.domain.user.user import UserCredentials, User


class UserFinderImpl(UserFinder):

    @inject.autoparams()
    def __init__(self, user_repository: UserRepository) -> None:
        self._user_repository = user_repository

    def find(self, user: UserCredentials) -> User:
        user_found: User = self._user_repository.find_by_email(user.username)
        if not user_found:
            raise Exception("Not found")
        if user.password != user_found.password:
            raise Exception("Password doesn't match")
        return user_found
