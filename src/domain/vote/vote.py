from typing import List
from fastapi import Body
from pydantic import BaseModel


class VoteBase(BaseModel):
    pass


class VoteCandidate(BaseModel):
    candidate_id: int
    number_of_votes: int


class VoteCreate(VoteBase):
    # id: int
    central_electoral: int = Body(default=1)
    election: int = Body(default=1)
    number_of_absentees: int
    blank_votes: int
    null_votes: int
    vote_candidate: List[VoteCandidate]
    is_quick_count: bool
    has_inconsistencies: bool
