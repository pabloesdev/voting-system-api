from typing import TypeAlias, List
from pydantic import BaseModel


class ElectionBase(BaseModel):
    pass


class Election(ElectionBase):
    id: int
    type: str
    date: str


Elections: TypeAlias = List[Election]
