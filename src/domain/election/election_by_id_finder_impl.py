import inject
from src.domain.election.ports.input.election_by_id_finder import ElectionByIdFinder
from src.domain.election.ports.output.election_repository import ElectionRepository
from src.domain.election.election import Election


class ElectionByIdFinderImpl(ElectionByIdFinder):

    @inject.autoparams()
    def __init__(self, election_repository: ElectionRepository) -> None:
        self._election_repository = election_repository

    def find(self, election_id: int) -> Election:
        election_found: Election = self._election_repository.find_by_id(election_id)
        if not election_found:
            raise Exception("Not found")
        return election_found
