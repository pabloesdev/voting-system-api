from abc import ABC, abstractmethod
from src.domain.election.election import Election


class ElectionByIdFinder(ABC):

    @abstractmethod
    def find(self, election_id: int) -> Election: ...
