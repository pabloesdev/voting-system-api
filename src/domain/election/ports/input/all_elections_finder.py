from abc import ABC, abstractmethod
from src.domain.election.election import Elections


class AllElectionsFinder(ABC):

    @abstractmethod
    def find(self) -> Elections: ...
