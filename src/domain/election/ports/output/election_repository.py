from abc import ABC, abstractmethod
from src.domain.election.election import Election, Elections


class ElectionRepository(ABC):

    @abstractmethod
    def find_by_id(self, election_id: int) -> Election: ...

    @abstractmethod
    def find_all(self) -> Elections: ...
