import inject
from src.domain.election.ports.input.all_elections_finder import AllElectionsFinder
from src.domain.election.ports.output.election_repository import ElectionRepository
from src.domain.election.election import Elections


class AllElectionsFinderImpl(AllElectionsFinder):

    @inject.autoparams()
    def __init__(self, election_repository: ElectionRepository) -> None:
        self._election_repository = election_repository

    def find(self) -> Elections:
        elections_found: Elections = self._election_repository.find_all()
        return elections_found
