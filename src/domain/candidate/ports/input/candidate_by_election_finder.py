from abc import ABC, abstractmethod
from src.domain.candidate.candidate import Candidate


class CandidateByElectionFinder(ABC):

    @abstractmethod
    def find(self, election_id: int) -> Candidate: ...
