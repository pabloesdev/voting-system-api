from abc import ABC, abstractmethod
from src.domain.candidate.candidate import Candidate


class CandidateByIdFinder(ABC):

    @abstractmethod
    def find(self, candidate_id: int) -> Candidate: ...
