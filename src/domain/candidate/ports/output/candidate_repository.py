from abc import ABC, abstractmethod
from src.domain.candidate.candidate import Candidates, Candidate


class CandidateRepository(ABC):

    @abstractmethod
    def find_by_id(self, candidate_id: int) -> Candidate: ...

    @abstractmethod
    def find_by_election(self, election_id: int) -> Candidates: ...
