from enum import Enum
from typing import TypeAlias, List
from pydantic import BaseModel


class CandidateType(str, Enum):
    PRESIDENTE = 'presidente'
    ASAMBLEISTA = 'asambleista'


class CandidateBase(BaseModel):
    pass


class Candidate(CandidateBase):
    id: int
    name: str
    type: CandidateType
    identification: str
    place_of_birth: str
    image: str


Candidates: TypeAlias = List[Candidate]
