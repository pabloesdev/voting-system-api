import inject
from src.domain.candidate.ports.input.candidate_by_id_finder import CandidateByIdFinder
from src.domain.candidate.ports.output.candidate_repository import CandidateRepository
from src.domain.candidate.candidate import Candidate


class CandidateByIdFinderImpl(CandidateByIdFinder):

    @inject.autoparams()
    def __init__(self, candidate_repository: CandidateRepository) -> None:
        self._candidate_repository = candidate_repository

    def find(self, candidate_id: int) -> Candidate:
        candidate_found: Candidate = self._candidate_repository.find_by_id(candidate_id)
        if not candidate_found:
            raise Exception("Not found")
        return candidate_found
