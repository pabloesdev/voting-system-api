import inject
from src.domain.candidate.ports.input.candidate_by_id_finder import CandidateByIdFinder
from src.domain.candidate.ports.output.candidate_repository import CandidateRepository
from src.domain.candidate.candidate import Candidates


class CandidateByElectionFinderImpl(CandidateByIdFinder):

    @inject.autoparams()
    def __init__(self, candidate_repository: CandidateRepository) -> None:
        self._candidate_repository = candidate_repository

    def find(self, election_id: int) -> Candidates:
        candidates: Candidates = self._candidate_repository.find_by_election(election_id)
        return candidates
