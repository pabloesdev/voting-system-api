import inject
from src.domain.central_electoral.central_electoral import CentralElectoral
from src.domain.central_electoral.ports.input.central_electoral_by_id_finder import CentralElectoralByIdFinder
from src.domain.central_electoral.ports.output.central_electoral_repository import CentralElectoralRepository


class CentralElectoralByIdFinderImpl(CentralElectoralByIdFinder):

    @inject.autoparams()
    def __init__(self, central_electoral_repository: CentralElectoralRepository) -> None:
        self._central_electoral_repository = central_electoral_repository

    def find(self, central_electoral_id: int) -> CentralElectoral:
        central_electoral: CentralElectoral = self._central_electoral_repository.find_by_id(central_electoral_id)
        if not central_electoral:
            raise Exception("Not found")
        return central_electoral
