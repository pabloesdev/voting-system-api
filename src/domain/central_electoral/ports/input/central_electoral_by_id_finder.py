from abc import ABC, abstractmethod
from src.domain.central_electoral.central_electoral import CentralElectoral


class CentralElectoralByIdFinder(ABC):

    @abstractmethod
    def find(self, central_electoral_id: int) -> CentralElectoral: ...
