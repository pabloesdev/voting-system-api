from abc import ABC, abstractmethod
from src.domain.central_electoral.central_electoral import CentralElectorals


class AllCentralElectoralFinder(ABC):

    @abstractmethod
    def find(self) -> CentralElectorals: ...
