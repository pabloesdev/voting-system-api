from abc import ABC, abstractmethod

from src.domain.central_electoral.central_electoral import CentralElectoral, CentralElectorals


class CentralElectoralRepository(ABC):

    @abstractmethod
    def find_all(self) -> CentralElectorals: ...

    @abstractmethod
    def find_by_id(self, central_electoral_id: int) -> CentralElectoral: ...
