import inject
from src.domain.central_electoral.ports.output.central_electoral_repository import CentralElectoralRepository
from src.domain.central_electoral.central_electoral import CentralElectorals
from src.domain.central_electoral.ports.input.all_central_electoral_finder import AllCentralElectoralFinder


class AllCentralElectoralFinderImpl(AllCentralElectoralFinder):

    @inject.autoparams()
    def __init__(self, central_electoral_repository: CentralElectoralRepository) -> None:
        self._central_electoral_repository = central_electoral_repository

    def find(self) -> CentralElectorals:
        result: CentralElectorals = self._central_electoral_repository.find_all()
        return result
