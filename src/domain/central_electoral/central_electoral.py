from typing import TypeAlias, List
from pydantic import BaseModel


class CentralElectoralBase(BaseModel):
    pass


class CentralElectoral(CentralElectoralBase):
    id: int
    province: str
    district: str
    city: str
    parish: str
    number_of_registered: int
    zone: str
    junta: str


CentralElectorals: TypeAlias = List[CentralElectoral]
