from src import get_env_var
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from typing import TypeVar, Callable, Any, cast

FuncT = TypeVar("FuncT", bound=Callable[..., Any])


def database_engine() -> Callable[
    [FuncT],
    Callable[[FuncT], FuncT]
]:
    def decorator(function: FuncT) -> Callable[[FuncT], FuncT]:
        def wrapper(*args: Any, **kwargs: Any) -> FuncT:
            database_uri = get_env_var("DATABASE_URL")
            kwargs['engine'] = create_engine(database_uri).connect()
            return function(*args, **kwargs)
        return cast(FuncT, wrapper)

    return decorator


class DatabaseSession(Session):
    ...
