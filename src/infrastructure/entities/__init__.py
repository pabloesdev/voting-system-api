from typing import Any
from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import DeclarativeMeta

Base: Any = declarative_base()

from src.infrastructure.entities.user_entity import UserEntity  # noqa: F401 E402
from src.infrastructure.entities.candidate_entity import CandidateEntity  # noqa: F401 E402
from src.infrastructure.entities.election_entity import ElectionEntity, ElectionCandidateEntity  # noqa: F401 E402
from src.infrastructure.entities.central_electoral_entity import CentralElectoralEntity  # noqa: F401 E402
from src.infrastructure.entities.vote_entity import VoteEntity, VoteCandidateEntity  # noqa: F401 E402
