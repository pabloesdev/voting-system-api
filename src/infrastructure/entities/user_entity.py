from sqlalchemy import Column, Integer, VARCHAR
from src.infrastructure.entities import Base
from src.infrastructure.entities.base_entity import BaseEntity


class UserEntity(Base, BaseEntity):
    __tablename__ = 'users'
    id = Column(Integer(), primary_key=True)
    name = Column(VARCHAR(length=25), nullable=False)
    username = Column(VARCHAR(length=30), nullable=False)
    password = Column(VARCHAR(length=200), nullable=False)
    role = Column(VARCHAR(length=50))
