from sqlalchemy import Column, Integer, VARCHAR
from src.infrastructure.entities import Base
from src.infrastructure.entities.base_entity import BaseEntity


class CentralElectoralEntity(Base, BaseEntity):
    __tablename__ = 'central_electorals'
    id = Column(Integer(), primary_key=True)
    province = Column(VARCHAR(length=50), nullable=False)
    district = Column(VARCHAR(length=50))
    city = Column(VARCHAR(length=50), nullable=False)
    parish = Column(VARCHAR(length=50), nullable=False)
    number_of_registered = Column(Integer, nullable=False)
    zone = Column(VARCHAR(length=50))
    junta = Column(VARCHAR(length=50))
