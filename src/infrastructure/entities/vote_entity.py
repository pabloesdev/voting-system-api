from sqlalchemy import Column, Integer, VARCHAR, ForeignKey, Boolean
from src.infrastructure.entities import Base
from src.infrastructure.entities.base_entity import BaseEntity
from typing import Any


class VoteEntity(Base, BaseEntity):
    __tablename__ = 'votes'
    id = Column(Integer(), primary_key=True)
    central_electoral = Column(Integer(), ForeignKey('central_electorals.id'))
    election = Column(Integer(), ForeignKey('elections.id'))
    number_of_absentees = Column(VARCHAR(length=15), nullable=False)
    blank_votes = Column(Integer(), nullable=False)
    null_votes = Column(Integer(), nullable=False)
    is_quick_count: Any = Column(Boolean())
    has_inconsistencies: Any = Column(Boolean())


class VoteCandidateEntity(Base, BaseEntity):
    __tablename__ = 'votes_candidate'
    id = Column(Integer(), primary_key=True)
    vote = Column(Integer(), ForeignKey('votes.id'))
    number_of_votes = Column(Integer())
    candidate = Column(Integer(), ForeignKey('candidates.id'))
