from sqlalchemy import Column, Integer, VARCHAR, Text
from src.infrastructure.entities import Base
from src.infrastructure.entities.base_entity import BaseEntity


class CandidateEntity(Base, BaseEntity):
    __tablename__ = 'candidates'
    id = Column(Integer(), primary_key=True)
    name = Column(VARCHAR(length=25), nullable=False)
    type = Column(VARCHAR(length=15), default="presidente")
    identification = Column(VARCHAR(length=15), nullable=False)
    place_of_birth = Column(VARCHAR(length=50), nullable=False)
    image = Column(Text, nullable=False)
