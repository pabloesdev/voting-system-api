from sqlalchemy import Column, Integer, VARCHAR, DateTime, func, ForeignKey
from src.infrastructure.entities import Base
from src.infrastructure.entities.base_entity import BaseEntity


class ElectionEntity(Base, BaseEntity):
    __tablename__ = 'elections'
    id = Column(Integer(), primary_key=True)
    type = Column(VARCHAR(length=50), nullable=False)
    date = Column(DateTime(timezone=True), server_default=func.now())


class ElectionCandidateEntity(Base, BaseEntity):
    __tablename__ = 'elections_candidates'
    id = Column(Integer(), primary_key=True)
    election = Column(Integer(), ForeignKey('elections.id'))
    candidate = Column(Integer(), ForeignKey('candidates.id'))
