from jwt import encode, decode
from src import get_env_var


class TokenManager(object):

    """
    Class to manage token in the application
    """

    def __init__(self) -> None:
        self._secret = get_env_var("TOKEN_SECRET")

    def generate_token(self, data: dict) -> str:
        token: str = encode(payload=data, key=self._secret, algorithm="HS256")
        return token

    def decode_token(self, token: str) -> str:
        data = decode(token, key=self._secret, algorithms=["HS256"])
        return data
