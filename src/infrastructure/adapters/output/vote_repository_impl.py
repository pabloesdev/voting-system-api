from abc import ABC, abstractmethod
from typing import Any, Optional, List
from sqlalchemy import text
from sqlalchemy.orm import Session
from src.domain.vote.vote import VoteCreate
from src.infrastructure.database.db_engine_config import database_engine
from src.infrastructure.entities.vote_entity import VoteCandidateEntity, VoteEntity


class VoteRepository(ABC):

    @abstractmethod
    def get_votes(self, province: str | None, only_quick_count: bool | None) -> List: ...

    @abstractmethod
    def create(self, vote: VoteCreate) -> None: ...


class VoteRepositoryImpl(VoteRepository):

    def __init__(self) -> None:
        pass

    @database_engine()
    def get_votes(self, province: str | None, only_quick_count: bool | None, **kwargs: Optional[Any]) -> dict:
        votes_result = list()
        with Session(bind=kwargs.get("engine")) as session:
            sql = """
                SELECT
                    c.id as candidate_id,
                    c.name as candidate_name,
                    c.image as candidate_image,
                    SUM(vc.number_of_votes) as total_votes,
                    (SUM(vc.number_of_votes) * 100.0 / SUM(SUM(vc.number_of_votes)) OVER ()) as vote_percentage
                FROM
                    votes_candidate vc
                JOIN
                    candidates c ON vc.candidate = c.id
                JOIN
                    votes v ON vc.vote = v.id
                LEFT JOIN
                    central_electorals ce ON v.central_electoral = ce.id
                WHERE
                    (:province_filter IS NULL OR ce.province = :province_filter)
                AND
                    (:only_quick_count IS NULL OR v.is_quick_count = :only_quick_count)
                GROUP BY
                    c.id, c.name, c.image
                ORDER BY
                    c.id ASC
            """
            result = session.execute(
                text(sql),
                {"province_filter": province,
                 "only_quick_count": only_quick_count
                 }
            )
            for row in result:
                candidate_id, candidate_name, candidate_image, total_votes, votes_percentage = row
                votes_result.append({
                    "candidate": {
                        "id": candidate_id,
                        "name": candidate_name,
                        "image": candidate_image,
                    },
                    "votes": {
                        "total": total_votes,
                        "percentage": float(f"{votes_percentage:.2f}"),
                    }
                })
                print(f"Candidato ID: {candidate_id}, Total de Votos: {total_votes}")

            with Session(bind=kwargs.get('engine')) as session2:
                sql2 = """
                    SELECT
                        COALESCE(SUM(CASE WHEN vc.number_of_votes > 0 THEN vc.number_of_votes ELSE 0 END), 0) as valid_votes,
                        COALESCE(SUM(CASE WHEN v.null_votes > 0 THEN v.blank_votes ELSE 0 END), 0) as null_votes,
                        COALESCE(SUM(CASE WHEN v.blank_votes > 0 THEN v.blank_votes ELSE 0 END), 0) as blank_votes,
                        COALESCE(SUM(CASE WHEN v.number_of_absentees :: Integer > 0 THEN v.number_of_absentees :: Integer ELSE 0 END), 0)
                            as number_of_absentees
                    FROM
                        votes_candidate vc
                    JOIN
                        candidates c ON vc.candidate = c.id
                    JOIN
                        votes v ON vc.vote = v.id
                    LEFT JOIN
                        central_electorals ce ON v.central_electoral = ce.id
                    WHERE
                        (:province_filter IS NULL OR ce.province = :province_filter)
                    AND
                        (:only_quick_count IS NULL OR v.is_quick_count = :only_quick_count)
                """

                result2 = session2.execute(
                    text(sql2),
                    {"province_filter": province,
                     "only_quick_count": only_quick_count
                     }
                )
                row2: Any = result2.fetchone()

            with Session(bind=kwargs.get('engine')) as session3:
                sql3 = """
                    SELECT
                        COUNT(*) FILTER (WHERE v.has_inconsistencies = TRUE) as votes_with_has_inconsistencies,
                        COUNT(*) FILTER (WHERE v.has_inconsistencies = FALSE) as valids,
                        COUNT(*) as total_votes
                    FROM votes v
                """

                result3 = session3.execute(
                    text(sql3),
                    {"province_filter": province}
                )
                row3: Any = result3.fetchone()

            response = {
                "voters_total": {
                    "total": row2.valid_votes + row2.null_votes + row2.blank_votes,
                    "total_number_of_absentees": row2.number_of_absentees,
                    "total_voters": row2.valid_votes + row2.null_votes + row2.blank_votes
                },

                "votes_info": {
                    "with_inconsistencies": row3[0],
                    "valids": row3[1],
                    "pending": 4000 - row3[2]
                },

                "votes_total": {
                    "total_valid_votes": row2.valid_votes,
                    # "percentage_valid_votes": row2.valid_votes * 100 / (row2.valid_votes + row2.null_votes + row2.blank_votes),
                    "total_blank_votes": row2.blank_votes,
                    # "percentage_blank_votes": row2.blank_votes * 100 / (row2.valid_votes + row2.null_votes + row2.blank_votes),
                    "total_null_votes": row2.null_votes,
                    # "percentage_null_votes": row2.null_votes * 100 / (row2.valid_votes + row2.null_votes + row2.blank_votes),
                },
                "candidates_result": votes_result
            }
            return response

    @database_engine()
    def create(self, vote: VoteCreate, **kwargs: Optional[Any]) -> None:
        with Session(bind=kwargs.get("engine")) as session:
            vote_entity = VoteEntity(
                central_electoral=vote.central_electoral,
                election=vote.election,
                number_of_absentees=vote.number_of_absentees,
                blank_votes=vote.blank_votes,
                null_votes=vote.null_votes,
                is_quick_count=vote.is_quick_count,
                has_inconsistencies=vote.has_inconsistencies
            )
            session.add(vote_entity)
            session.commit()
            for vc in vote.vote_candidate:
                vote_candidate_entity = VoteCandidateEntity(
                    vote=vote_entity.id,
                    candidate=vc.candidate_id,
                    number_of_votes=vc.number_of_votes
                )
                session.add(vote_candidate_entity)
                session.commit()
