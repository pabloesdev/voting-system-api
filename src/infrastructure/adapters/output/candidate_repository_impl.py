from typing import List, Optional, Any
from src.domain.candidate.ports.output.candidate_repository import CandidateRepository
from src.domain.candidate.candidate import Candidate, Candidates
from src.infrastructure.database.db_engine_config import database_engine
from sqlalchemy.orm import Session
from src.infrastructure.entities.candidate_entity import CandidateEntity
from src.infrastructure.entities.election_entity import ElectionCandidateEntity


class CandidateRepositoryImpl(CandidateRepository):

    def __init__(self) -> None:
        super().__init__()

    @database_engine()
    def find_by_id(self, candidate_id: int, **kwargs: Optional[Any]) -> Candidate:
        with Session(bind=kwargs.get("engine")) as session:
            candidate: CandidateEntity | None = session.query(CandidateEntity).get(candidate_id)
            if candidate is None:
                raise Exception("Candidate not found")
            return Candidate(
                id=candidate.id,
                name=candidate.name,
                type=candidate.type,
                identification=candidate.identification,
                place_of_birth=candidate.place_of_birth,
                image=candidate.image
            )

    @database_engine()
    def find_by_election(self, election_id: str, **kwargs: Optional[Any]) -> Candidates:
        with Session(bind=kwargs.get("engine")) as session:
            candidates: List[CandidateEntity] = (
                session.query(CandidateEntity)
                .join(ElectionCandidateEntity, ElectionCandidateEntity.candidate == CandidateEntity.id)
                .filter(ElectionCandidateEntity.election == election_id)
                .all()
            )
            return [Candidate(
                id=c.id,
                name=c.name,
                type=c.type,
                identification=c.identification,
                place_of_birth=c.place_of_birth,
                image=c.image
            ) for c in candidates]
