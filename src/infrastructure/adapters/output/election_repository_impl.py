from typing import List, Optional, Any
from src.domain.election.election import Election, Elections
from src.domain.election.ports.output.election_repository import ElectionRepository
from src.infrastructure.database.db_engine_config import database_engine
from sqlalchemy.orm import Session
from src.infrastructure.entities.election_entity import ElectionEntity


class ElectionRepositoryImpl(ElectionRepository):

    def __init__(self) -> None:
        super().__init__()

    @database_engine()
    def find_by_id(self, election_id: int, **kwargs: Optional[Any]) -> Election:
        with Session(bind=kwargs.get("engine")) as session:
            election: ElectionEntity | None = session.query(ElectionEntity).get(election_id)
            if election is None:
                raise Exception("Election not found")
            return Election(
                id=election.id,
                type=election.type,
                date=f'{election.date:%Y-%m-%d %H:%M}'
            )

    @database_engine()
    def find_all(self, **kwargs: Optional[Any]) -> Elections:
        with Session(bind=kwargs.get("engine")) as session:
            elections: List[ElectionEntity] = session.query(ElectionEntity).all()
            return [Election(
                id=e.id,
                type=e.type,
                date=f'{e.date:%Y-%m-%d %H:%M}'
            ) for e in elections]
