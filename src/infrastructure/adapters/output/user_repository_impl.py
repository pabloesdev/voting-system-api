from typing import Optional, Any
from src.domain.user.ports.output.user_repository import UserRepository
from src.domain.user.user import UserCreate, UserPasswordUpdate, User
from src.infrastructure.database.db_engine_config import database_engine
from sqlalchemy.orm import Session
from sqlalchemy import insert
from src.infrastructure.entities.user_entity import UserEntity


class UserRepositoryImpl(UserRepository):

    def __init__(self) -> None:
        super().__init__()

    @database_engine()
    def create(self, user: UserCreate, **kwargs: Optional[Any]) -> None:
        with Session(bind=kwargs.get("engine")) as session:
            create = insert(UserEntity).values(**user.model_dump())
            session.execute(create)
            session.commit()

    @database_engine()
    def find_by_email(self, email: str, **kwargs: Optional[Any]) -> User:
        with Session(bind=kwargs.get("engine")) as session:
            user: UserEntity | None = session.query(UserEntity).filter_by(username=email).first()
            if user is None:
                raise Exception("User not found")
            return User(  # add automapper later
                id=user.id,
                name=user.name,
                username=user.username,
                password=user.password,
                role=user.role
            )

    def update_password(self, user: UserPasswordUpdate) -> str:
        return "password changed"
