from typing import Any, Optional, List
from src.domain.central_electoral.central_electoral import CentralElectoral, CentralElectorals
from src.domain.central_electoral.ports.output.central_electoral_repository import CentralElectoralRepository
from src.infrastructure.database.db_engine_config import database_engine
from sqlalchemy.orm import Session

from src.infrastructure.entities.central_electoral_entity import CentralElectoralEntity


class CentralElectoralRepositoryImpl(CentralElectoralRepository):

    def __init__(self) -> None:
        super().__init__()

    @database_engine()
    def find_all(self, **kwargs: Optional[Any]) -> CentralElectorals:
        with Session(bind=kwargs.get("engine")) as session:
            central_electorals: List[CentralElectoralEntity] = session.query(CentralElectoralEntity).all()
            return [CentralElectoral(
                id=c.id,
                province=c.province,
                district=c.district,
                city=c.city,
                parish=c.parish,
                number_of_registered=c.number_of_registered,
                zone=c.zone,
                junta=c.junta
            ) for c in central_electorals]

    @database_engine()
    def find_by_id(self, central_electoral_id: int, **kwargs: Optional[Any]) -> CentralElectoral:
        with Session(bind=kwargs.get("engine")) as session:
            central_electoral: CentralElectoralEntity | None = session.query(CentralElectoralEntity).get(central_electoral_id)
            if central_electoral is None:
                raise Exception("Central Electoral not found")
            return CentralElectoral(
                id=central_electoral.id,
                province=central_electoral.province,
                district=central_electoral.district,
                city=central_electoral.city,
                parish=central_electoral.parish,
                number_of_registered=central_electoral.number_of_registered,
                zone=central_electoral.zone,
                junta=central_electoral.junta
            )
