# Run application
python manage.py run server host=0.0.0.0 --port=5000

# Migrations
alembic revision --autogenerate
alembic upgrade head

# Enter to app container
docker compose -f docker-compose.yml exec app bash
alembic revision --autogenerate
alembic upgrade head